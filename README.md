# ETH Template for LaTeX letters

## Prerequisite

You need an up to date and working LaTeX installation.

## Installation

You can just download this [ZIP](https://gitlab.ethz.ch/mmarcio/ethbrief3/-/archive/master/ethbrief3-master.zip) file, extract it and begin to work in the extracted directory or if you prefer you can install the files on your system, so that your LaTeX installation find the needed files (recommended).

### Windows

TODO

### Linux & macOS with TeX Live

Download and extract the ZIP file and copy the class file (`.cls`) and the `assets` directory in one of the  following two ways:

#### For all users on the system

For this you'll need admin rights:

```bash
sudo mkdir -m775 -p $(kpsewhich -var-value=TEXMFLOCAL)/tex/latex/ethbrief3
sudo mv ethbrief3.cls $(kpsewhich -var-value=TEXMFLOCAL)/tex/latex/ethbrief3/
sudo mv assets $(kpsewhich -var-value=TEXMFLOCAL)/tex/latex/ethbrief3/
sudo mktexlsr /usr/share/texlive/texmf-local
```

#### Only for you

```bash
mkdir -m775 -p $(kpsewhich -var-value=TEXMFHOME)/tex/latex/ethbrief3
mv ethbrief3.cls $(kpsewhich -var-value=TEXMFHOME)/tex/latex/ethbrief3/
mv assets $(kpsewhich -var-value=TEXMFHOME)/tex/latex/ethbrief3/
```

## Usage

Now that you installed the ETH Template for LaTeX letters you still need the following files from the ZIP package:

```
sample-address.sty  -->  example of an address-file
sample-letter.tex   -->  example of a letter
```

Then:

* rename the file `sample-address.sty` to something more appropriate (like `YourFullName.sty`) and update it with your address and the other information
* modify the `sample-letter.tex` , change also the line with `\usepackage{sample-address}` to `\usepackage{YourFullName}` and compile it with `pdflatex`

The result will looks like this:

![Sample letter](sample-letter.png)
